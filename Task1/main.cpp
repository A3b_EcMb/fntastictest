#include "iostream"
#include "string"
#include "unordered_map"
#include "cctype"

using namespace std;

int main()
{
	string input;

	cout << "Enter input string:" << endl;
	getline(cin, input);

	unordered_map<char, int> map;

	for (int i = 0; i < input.size(); i++)
	{
		char current = tolower(input[i]);

		if (map.find(current) == map.end())
			map[current] = 1;
		else
			map[current] += 1;
	}

	for (int i = 0; i < input.size(); i++)
	{
		char current = tolower(input[i]);

		int count = map[current];
		input[i] = count > 1 ? ')' : '(';
	}

	cout << input;

	return 0;
}