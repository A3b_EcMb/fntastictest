#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Finish.generated.h"

UCLASS()
class TEST_API AFinish : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFinish();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddTurtle(AActor* turtle);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void PlayFinishFX();

private:
	const float FinishDistance = 10;

	TArray<AActor*> Turtles;
};
