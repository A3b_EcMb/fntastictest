#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Turtle.generated.h"

UCLASS()
class TEST_API ATurtle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATurtle();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Speed;

	void StartMoveToTarget(AActor* target);
	void Stop();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void PlaySteps();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void StopSteps();

private:
	class AActor* Target;
	bool HasTarget;
};