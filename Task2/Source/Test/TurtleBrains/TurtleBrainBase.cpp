#include "TurtleBrainBase.h"

// Sets default values for this component's properties
UTurtleBrainBase::UTurtleBrainBase()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// PrimaryComponentTick.bCanEverTick = false;
	// PrimaryComponentTick.bStartWithTickEnabled = true;

	// ...
}


// Called when the game starts
void UTurtleBrainBase::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UTurtleBrainBase::SetUp(ATurtle* turtle, AActor* target, AActor* spawner)
{
	Turtle = turtle;
	Target = target;
	Spawner = spawner;
	IsActive = true;

	RegisterComponent();
}