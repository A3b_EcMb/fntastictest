
#include "SimpleTurtleBrain.h"

// Called every frame
void USimpleTurtleBrain::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USimpleTurtleBrain::SetUp(ATurtle* turtle, AActor* target, AActor* spawner)
{
	Super::SetUp(turtle, target, spawner);
	
	turtle->StartMoveToTarget(target);
}