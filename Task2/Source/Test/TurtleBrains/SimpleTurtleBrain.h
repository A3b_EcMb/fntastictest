#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TurtleBrainBase.h"
#include "SimpleTurtleBrain.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TEST_API USimpleTurtleBrain : public UTurtleBrainBase
{
	GENERATED_BODY()

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void SetUp(ATurtle* turtle, AActor* target, AActor* spawner) override;
};
