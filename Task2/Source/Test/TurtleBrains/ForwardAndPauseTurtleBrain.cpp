#include "ForwardAndPauseTurtleBrain.h"

// Called every frame
void UForwardAndPauseTurtleBrain::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!IsActive) return;

	Time -= DeltaTime;
	if (Time > 0) return;

	if (IsMoving)
	{
		Turtle->Stop();
		Time = PauseTime;
		IsMoving = false;
	}
	else
	{
		Turtle->StartMoveToTarget(Target);
		Time = MoveTime;
		IsMoving = true;
	}
}

void UForwardAndPauseTurtleBrain::SetUp(ATurtle* turtle, AActor* target, AActor* spawner)
{
	Super::SetUp(turtle, target, spawner);

	turtle->StartMoveToTarget(target);
	Time = MoveTime;
	IsMoving = true;
}