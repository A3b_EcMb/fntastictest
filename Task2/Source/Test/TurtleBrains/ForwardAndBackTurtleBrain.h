#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TurtleBrainBase.h"
#include "ForwardAndBackTurtleBrain.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TEST_API UForwardAndBackTurtleBrain : public UTurtleBrainBase
{
	GENERATED_BODY()

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void SetUp(ATurtle* turtle, AActor* target, AActor* spawner) override;

private:
	const float ForwardTime = 1;
	const float BackTime = 0.5f;

	float Time;
	bool IsMovingForward;
};
