#include "ForwardAndBackTurtleBrain.h"

// Called every frame
void UForwardAndBackTurtleBrain::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	if (!IsActive) return;

	Time -= DeltaTime;
	if (Time > 0) return;

	if (IsMovingForward)
	{
		Turtle->StartMoveToTarget(Spawner);
		Time = BackTime;
		IsMovingForward = false;
	}
	else
	{
		Turtle->StartMoveToTarget(Target);
		Time = ForwardTime;
		IsMovingForward = true;
	}
}

void UForwardAndBackTurtleBrain::SetUp(ATurtle* turtle, AActor* target, AActor* spawner)
{
	Super::SetUp(turtle, target, spawner);

	turtle->StartMoveToTarget(target);
	Time = ForwardTime;
	IsMovingForward = true;
}