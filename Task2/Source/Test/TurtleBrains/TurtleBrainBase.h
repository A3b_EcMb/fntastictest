#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Test/Turtle.h"
#include "TurtleBrainBase.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), abstract )
class TEST_API UTurtleBrainBase : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTurtleBrainBase();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	virtual void SetUp(ATurtle* turtle, AActor* target, AActor* spawner);

protected:
	class ATurtle* Turtle;
	class AActor* Target;
	class AActor* Spawner;
	bool IsActive;
};
