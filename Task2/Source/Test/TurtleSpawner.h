#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Turtle.h"
#include "Finish.h"
#include "TurtleBrains/TurtleBrainBase.h"
#include "TurtleSpawner.generated.h"

UCLASS()
class TEST_API ATurtleSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATurtleSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void PlaySound();

	UFUNCTION(BlueprintCallable)
	void SpawnTurtle();

	UPROPERTY(EditAnywhere, Category = "TurtleSpawn")
	TSubclassOf<ATurtle> TurtleToSpawn;

	UPROPERTY(EditAnywhere, Category = "TurtleSpawn")
	class AFinish* Finish;

	UPROPERTY(EditAnywhere, Category = "TurtleSpawn")
	TSubclassOf<UTurtleBrainBase> Brain;
};