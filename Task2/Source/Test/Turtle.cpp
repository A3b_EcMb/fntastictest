#include "Turtle.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATurtle::ATurtle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ATurtle::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATurtle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!HasTarget) return;

	FVector currentLocation = GetActorLocation();
	FVector targetLocation = Target->GetActorLocation();

	FVector direction = targetLocation - currentLocation;
	direction = direction.GetSafeNormal();

	float travelDistance = Speed * DeltaTime;
	direction = direction * travelDistance;

	FRotator lookRotation = UKismetMathLibrary::FindLookAtRotation(currentLocation, targetLocation);

	SetActorLocationAndRotation(currentLocation + direction, lookRotation);
}

void ATurtle::StartMoveToTarget(AActor* target)
{
	HasTarget = true;
	Target = target;

	UE_LOG(LogTemp, Warning, TEXT("Turtle start moving"));

	PlaySteps();
	// TODO: start animation
}

void ATurtle::Stop()
{
	HasTarget = false;
	Target = nullptr;

	UE_LOG(LogTemp, Warning, TEXT("Turtle stop moving"));

	StopSteps();
	// TODO: stop animation
}

void ATurtle::PlaySteps_Implementation()
{

}

void ATurtle::StopSteps_Implementation()
{

}