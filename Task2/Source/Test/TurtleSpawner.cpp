#include "TurtleSpawner.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATurtleSpawner::ATurtleSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void ATurtleSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATurtleSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATurtleSpawner::SpawnTurtle()
{
	FVector spawnLocation = GetActorLocation();
	FVector targetLocation = Finish->GetActorLocation();
	FRotator spawnRotation = UKismetMathLibrary::FindLookAtRotation(spawnLocation, targetLocation);

	ATurtle* spawnedTurtle = (ATurtle*)GetWorld()->SpawnActor(TurtleToSpawn, &spawnLocation, &spawnRotation);

	USceneComponent* rootComponent = spawnedTurtle->GetRootComponent();

	UTurtleBrainBase* brain = NewObject<UTurtleBrainBase>(spawnedTurtle, Brain);
	spawnedTurtle->AddOwnedComponent(brain);
	brain->SetUp(spawnedTurtle, Finish, this);

	Finish->AddTurtle(spawnedTurtle);

	PlaySound();
}

void ATurtleSpawner::PlaySound_Implementation()
{

}