#include "Finish.h"

// Sets default values
AFinish::AFinish()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFinish::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFinish::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector finishLocation = GetActorLocation();

	for (int32 i = Turtles.Num() -1; i >= 0; i--)
	{
		AActor* turtle = Turtles[i];
		FVector turtleLocation = turtle->GetActorLocation();
		float distance = FVector::Dist(finishLocation, turtleLocation);
		if (distance > FinishDistance) continue;

		Turtles.RemoveAt(i);
		turtle->Destroy();

		PlayFinishFX();
		// TODO: add vfx
	}
}

void AFinish::AddTurtle(AActor* turtle)
{
	Turtles.Add(turtle);
}

void AFinish::PlayFinishFX_Implementation()
{

}